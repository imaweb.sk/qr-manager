<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** Auth routes */
Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false
]);

/** Public routes */
Route::get('/', [\App\Http\Controllers\MainController::class, 'index']);
Route::get('/open/{uuid}', [App\Http\Controllers\MainController::class, 'open']);

/** Admin routes */
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware("auth");
Route::post('/generate', [App\Http\Controllers\MainController::class, 'generate'])->middleware("auth");
Route::get('/qr/{id}/delete', [App\Http\Controllers\MainController::class, 'delete'])->middleware("auth");
Route::get('/qr/{id}/download', [App\Http\Controllers\MainController::class, 'download'])->middleware("auth");
Route::post('/qr/{id}/update', [App\Http\Controllers\MainController::class, 'update'])->middleware("auth");
