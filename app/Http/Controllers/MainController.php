<?php

namespace App\Http\Controllers;

use App\Models\Code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\SvgWriter;

class MainController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        if(!Auth::check()) {
            return redirect('/login');
        } else {
            return redirect('/home');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|void
     */
    public function generate(Request $request)
    {

        /** If URL is not valid */
        if (!filter_var($request->url, FILTER_VALIDATE_URL)) {
            abort(401);
        }

        /** If URL is valid */
        else {

            /** Generate UUID */
            $uuid = Uuid::uuid4();

            /** Store it in DB */
            $code = new Code();
            $code->uuid = $uuid;
            $code->link = $request->url;
            $code->save();

            /** Redirect back */
            return back();

        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {

        /** Delete file */
        Code::findOrFail($id)->delete();

        /** Redirect back */
        return back();

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {

        /** Update the code */
        $code = Code::findOrFail($id);
        $code->uuid = $request->uuid;
        $code->link = $request->link;
        $code->updated_at = Carbon::now();
        $code->save();

        /** Redirect back */
        return back();

    }

    /**
     * @param $id
     * @return void
     */
    public function download($id)
    {

        /** Get UUID */
        $code = Code::findOrFail($id);
        $uuid = $code->uuid;

        /** Generate link */
        $link = \Request::root() . "/open/" . $uuid;

        /** Generate QR Code */
        $qrCode = QrCode::create($link)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(1024)
            ->setMargin(0)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        /** Display image */
        $svg = new SvgWriter();
        $result = $svg->write($qrCode);

        /** Return view */
        return view("code")
            ->withCode($result->getDataUri())
            ->withDetails($code);

    }

    /**
     * @param $uuid
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function open($uuid)
    {

        /** Pull the code */
        $code = Code::where("uuid", $uuid)->first();

        /** If there is any problem with code */
        if(!$code) {
            return abort(404);
        }

        /** Update times */
        $code->scanned_times = (int) $code->scanned_times + 1;
        $code->scanned_last = Carbon::now();
        $code->save();

        /** Redirect to desired URL */
        return redirect($code->link);

    }

}
