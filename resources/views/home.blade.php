@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="card mb-3">
                    <form action="/generate" method="post">
                        @csrf
                        <div class="card-header">
                            Generate new code
                        </div>
                        <div class="card-body">
                            <input
                                type="url"
                                name="url"
                                class="form-control"
                                placeholder="Insert url"
                                required
                            >
                        </div>
                        <div class="card-footer">
                            <button
                                class="btn btn-secondary"
                            >
                                Generate code
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header">
                        Edit existing codes
                    </div>
                    <div class="card-body pb-2">
                        @if(count($codes) == 0)
                            <p class="mb-2">No codes available</p>
                        @endif
                        @foreach($codes as $code)
                            <form action="/qr/{{$code->id}}/update" method="POST">
                                @csrf
                                <div class="row mb-2">
                                    <div class="mb-2 col-12 col-lg-5 col-xxl-5">
                                        <input
                                            type="text"
                                            name="uuid"
                                            class="form-control"
                                            value="{{$code->uuid}}"
                                        >
                                    </div>
                                    <div class="mb-2 col-12 col-lg-4 col-xxl-5">
                                        <input
                                            type="text"
                                            name="link"
                                            class="form-control"
                                            value="{{$code->link}}"
                                        >
                                    </div>
                                    <div class="mb-2 col-12 col-lg-3 col-xxl-2">
                                        <button
                                            class="btn btn-success"
                                        >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2" viewBox="0 0 16 16">
                                                <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
                                            </svg>
                                        </button>
                                        <a
                                            href="/qr/{{$code->id}}/delete"
                                            class="btn btn-danger"
                                            onclick="if(!confirm('Are you sure?')) { return false }"
                                        >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                            </svg>
                                        </a>
                                        <a
                                            href="/qr/{{$code->id}}/download"
                                            class="btn btn-secondary"
                                        >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-qr-code" viewBox="0 0 16 16">
                                                <path d="M2 2h2v2H2V2Z"/>
                                                <path d="M6 0v6H0V0h6ZM5 1H1v4h4V1ZM4 12H2v2h2v-2Z"/>
                                                <path d="M6 10v6H0v-6h6Zm-5 1v4h4v-4H1Zm11-9h2v2h-2V2Z"/>
                                                <path d="M10 0v6h6V0h-6Zm5 1v4h-4V1h4ZM8 1V0h1v2H8v2H7V1h1Zm0 5V4h1v2H8ZM6 8V7h1V6h1v2h1V7h5v1h-4v1H7V8H6Zm0 0v1H2V8H1v1H0V7h3v1h3Zm10 1h-1V7h1v2Zm-1 0h-1v2h2v-1h-1V9Zm-4 0h2v1h-1v1h-1V9Zm2 3v-1h-1v1h-1v1H9v1h3v-2h1Zm0 0h3v1h-2v1h-1v-2Zm-4-1v1h1v-2H7v1h2Z"/>
                                                <path d="M7 12h1v3h4v1H7v-4Zm9 2v2h-3v-1h2v-1h1Z"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
