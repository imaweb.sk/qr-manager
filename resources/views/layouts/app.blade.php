<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Page title -->
    <title>QR Codes</title>

    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!-- Bootstrap CSS -->
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx"
        crossorigin="anonymous"
    >

    <!-- Bootstrap JS -->
    <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
        crossorigin="anonymous"
    ></script>

    <!-- Scripts & stylesheets -->
    @if(env('APP_DEBUG'))
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    @else
        @foreach(scandir(__DIR__ . "/../../../public/build/assets") as $file)
            @if(strpos($file, ".css") != false)
                <link rel="stylesheet" href="/build/assets/{{$file}}">
            @endif
            @if(strpos($file, ".js") != false)
                <script src="/build/assets/{{$file}}" defer></script>
            @endif
        @endforeach
    @endif


</head>
<body>
    <nav class="navbar navbar-expand-md navbar-light bg-white border-bottom mb-4">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                QR Codes
            </a>
            <div class="text-end">
                @guest
                    @if (Route::has('login'))
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    @endif
                    @if (Route::has('register'))
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                @else
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @endguest
            </div>
        </div>
    </nav>
    <main>
        @yield('content')
    </main>
</body>
</html>
