@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-3">
                <img
                    src="{{$code}}"
                    class="img-fluid"
                    onclick="save2()"
                    data-bs-toggle="tooltip"
                    data-bs-title="Click to download"
                    data-bs-placement="bottom"
                    style="cursor: pointer"
                >
                <script>
                    function save2() {
                        var a  = document.createElement('a');
                        a.href = "{{$code}}";
                        a.download = '{{$details->uuid}}.svg';
                        a.click()
                    }
                </script>
                <script>
                    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
                    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));
                </script>
            </div>
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        QR Code Details
                    </div>
                    <div class="card-body">
                        <p class="mb-0">
                            <b>Link:</b>
                            <a
                                href="{{$details->link}}"
                                target="_blank"
                                class="text-black text-decoration-none"
                            >{{$details->link}}</a>
                        </p>
                        <p class="mb-0">
                            <b>UUID:</b> {{$details->uuid}}
                        </p>
                        <p class="mb-0">
                            <b>Created at:</b> {{date("d.m.Y, H:i:s", strtotime($details->created_at))}}
                        </p>
                        <p class="mb-0">
                            <b>Updated at:</b> {{date("d.m.Y, H:i:s", strtotime($details->updated_at))}}
                        </p>
                        <p class="mb-0">
                            <b>Scanned:</b> {{$details->scanned_times}} times
                        </p>
                        <p class="mb-0">
                            @if($details->scanned_last)
                                <b>Last scanned:</b> {{date("d.m.Y, H:i:s", strtotime($details->scanned_last))}}
                            @else
                                <b>Last scanned:</b> Not scanned yet
                            @endif
                        </p>
                    </div>
                </div>
                <a href="/home">
                    <button class="btn btn-secondary mt-3">
                        <svg xmlns="http://www.w3.org/2000/svg" style="margin-top: -4px; margin-right: 4px" width="14" height="14" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                        </svg>
                        Return back
                    </button>
                </a>
            </div>
        </div>
    </div>
@endsection
